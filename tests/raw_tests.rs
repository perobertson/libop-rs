use std::collections::HashMap;

use libop::raw::vault_list;
use serde_json::Value;

#[tokio::test]
#[ignore = "op is not instlled in CI"]
async fn vault_list_returns_content() {
    let result = vault_list().await.unwrap();
    assert_ne!(result, "");

    // This is how raw json can be converted into a HashMap
    let vaults: Vec<HashMap<String, Value>> = serde_json::from_str(&result).unwrap();
    assert_ne!(vaults.len(), 0, "{:?}", vaults);
    // This is showing how it can be used
    let keys = ["id", "name", "content_version"];
    for mut vault in vaults {
        let mut map = HashMap::new();
        for key in keys {
            let (k, v) = vault.remove_entry(key).unwrap();
            map.insert(k, v);
        }
        assert_ne!(map.len(), 0, "{:?}", map);
    }
}
