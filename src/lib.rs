//! module for 1Password secret management by using op

pub mod errors;
pub mod raw;
pub mod untyped;

use errors::{MyError, ProcessError};
use serde::Deserialize;

/// Use 'op read' to read a value from 1Password at the location of the secret
/// reference.
///
/// See: <https://developer.1password.com/docs/cli/secret-references/>
#[deprecated(note = "please use 'raw::read_to_string' instead")]
pub async fn read_secret(reference: &str) -> Result<String, ProcessError> {
    raw::read_to_string(reference).await
}

#[derive(Deserialize)]
pub struct Item {
    pub id: String,
    pub title: String,
    #[serde(default)]
    pub tags: Vec<String>,
    pub version: usize,
    pub vault: ItemVault,
    pub category: String,
    pub last_edited_by: String,
    pub created_at: String,
    pub updated_at: String,
    // pub additional_information: String,
    #[serde(default)]
    pub sections: Vec<ItemSection>,
    #[serde(default)]
    pub fields: Vec<ItemField>,
    #[serde(default)]
    pub files: Vec<ItemFile>,
}
impl Item {
    pub fn try_from_json(value: &str) -> Result<Self, MyError> {
        let item: Item =
            serde_json::from_str(value).map_err(|err| MyError::Parse { source: err })?;
        Ok(item)
    }
}

#[derive(Deserialize)]
pub struct ItemFile {
    pub id: String,
    pub name: String,
    pub size: usize,
    pub content_path: String,
    pub section: Option<ItemFileSection>,
}
#[derive(Deserialize)]
pub struct ItemField {
    pub id: String,
    // pub type:String,
    // pub purpose: String,
    pub label: String,
    // pub value: String,
    pub reference: String,
}
#[derive(Deserialize)]
pub struct ItemFileSection {
    pub id: String,
}
#[derive(Deserialize)]
pub struct ItemSection {
    pub id: String,
}
#[derive(Deserialize)]
pub struct ItemVault {
    pub id: String,
    pub name: String,
}

#[derive(Deserialize)]
pub struct ItemListItem {
    pub id: String,
    pub title: String,
    // tags
    pub version: usize,
    // vault{id name}
    // category
    // last_edited_by
    // created_at
    // updated_at
    // additional_information
}

/// Vault returned from listing vaults
#[derive(Deserialize)]
pub struct VaultListItem {
    pub id: String,
    pub name: String,
    pub content_version: usize,
}

pub async fn item_get(vault_id: &str, item_id: &str) -> Result<Item, MyError> {
    let value = raw::item_get(vault_id, item_id)
        .await
        .map_err(|err| MyError::Op { source: err })?;
    Item::try_from_json(&value)
}

pub async fn item_list(vault_id: &str) -> Result<Vec<ItemListItem>, MyError> {
    let items = raw::item_list(vault_id)
        .await
        .map_err(|err| MyError::Op { source: err })?;

    let items: Vec<ItemListItem> =
        serde_json::from_str(&items).map_err(|err| MyError::Parse { source: err })?;

    Ok(items)
}

pub async fn vault_list() -> Result<Vec<VaultListItem>, MyError> {
    let vaults = raw::vault_list()
        .await
        .map_err(|err| MyError::Op { source: err })?;

    let vaults: Vec<VaultListItem> =
        serde_json::from_str(&vaults).map_err(|err| MyError::Parse { source: err })?;

    Ok(vaults)
}
