use std::collections::HashMap;

use serde_json::Value;

use crate::{errors::MyError, raw};

pub async fn item_get(vault_id: &str, item_id: &str) -> Result<HashMap<String, Value>, MyError> {
    let item = raw::item_get(vault_id, item_id)
        .await
        .map_err(|err| MyError::Op { source: err })?;

    let item: HashMap<String, Value> =
        serde_json::from_str(&item).map_err(|err| MyError::Parse { source: err })?;

    Ok(item)
}

pub async fn item_list(vault_id: &str) -> Result<Vec<HashMap<String, Value>>, MyError> {
    let items = raw::item_list(vault_id)
        .await
        .map_err(|err| MyError::Op { source: err })?;

    let items: Vec<HashMap<String, Value>> =
        serde_json::from_str(&items).map_err(|err| MyError::Parse { source: err })?;

    Ok(items)
}

pub async fn vault_get(vault_id: &str) -> Result<HashMap<String, Value>, MyError> {
    let vault = raw::vault_get(vault_id)
        .await
        .map_err(|err| MyError::Op { source: err })?;

    let vault: HashMap<String, Value> =
        serde_json::from_str(&vault).map_err(|err| MyError::Parse { source: err })?;

    Ok(vault)
}

pub async fn vault_list() -> Result<Vec<HashMap<String, Value>>, MyError> {
    let vaults = raw::vault_list()
        .await
        .map_err(|err| MyError::Op { source: err })?;

    let vaults: Vec<HashMap<String, Value>> =
        serde_json::from_str(&vaults).map_err(|err| MyError::Parse { source: err })?;

    Ok(vaults)
}
