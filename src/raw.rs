use std::{path::Path, process::Stdio};

use tokio::process::Command;
use tracing::{error, warn};
use wsl;

use crate::ProcessError;

/// Run 'op item get ITEM_ID --vault VAULT_ID --format json' and return the json string
///
/// See: <https://developer.1password.com/docs/cli/reference/management-commands/item/#item-get>
pub async fn item_get(vault_id: &str, item_id: &str) -> Result<String, ProcessError> {
    let args = [
        "item", "get", item_id, "--vault", vault_id, "--format", "json",
    ];
    run_cmd(&args).await
}

/// Run 'op item list --vault VAULT_ID --format json' and return the json string
///
/// See: <https://developer.1password.com/docs/cli/reference/management-commands/item/#item-list>
pub async fn item_list(vault_id: &str) -> Result<String, ProcessError> {
    let args = ["item", "list", "--vault", vault_id, "--format", "json"];
    run_cmd(&args).await
}

/// Use 'op read --no-newline --out-file FILE_PATH REFERENCE' to read a value
/// from 1Password at the location of the secret reference.
///
/// See: <https://developer.1password.com/docs/cli/secret-references/>
pub async fn read_to_file(reference: &str, path: &Path) -> Result<String, ProcessError> {
    let args = [
        "read",
        "--no-newline",
        "--out-file",
        &path.display().to_string(),
        reference,
    ];
    run_cmd(&args).await
}

/// Use 'op read --no-newline REFERENCE' to read a value from 1Password at the
/// location of the secret reference.
///
/// See: <https://developer.1password.com/docs/cli/secret-references/>
pub async fn read_to_string(reference: &str) -> Result<String, ProcessError> {
    let args = ["read", "--no-newline", reference];
    run_cmd(&args).await
}

async fn run_cmd(args: &[&str]) -> Result<String, ProcessError> {
    // Use the windows version when running in WSL because that links to the desktop app
    let program = if wsl::is_wsl() { "op.exe" } else { "op" };
    let mut cmd = Command::new(program);
    cmd.args(args);
    cmd.stdout(Stdio::piped());
    cmd.stderr(Stdio::piped());
    let child = match cmd.spawn() {
        Err(e) => {
            return Err(ProcessError::Io {
                msg: "failed to start".to_string(),
                cmd: format!("{} {}", program, args.join(" ")),
                source: e,
            });
        }
        Ok(child) => child,
    };
    let output = match child.wait_with_output().await {
        Err(e) => {
            return Err(ProcessError::Io {
                msg: "failed to run".to_string(),
                cmd: format!("{} {}", program, args.join(" ")),
                source: e,
            });
        }
        Ok(out) => out,
    };
    let stderr = match String::from_utf8(output.stderr) {
        Err(e) => {
            return Err(ProcessError::Read {
                msg: "failed to read stderr".to_string(),
                source: e,
            });
        }
        Ok(val) => val,
    };
    if !stderr.is_empty() {
        error!("{}", stderr);
    }
    if output.status.success() {
        let stdout = match String::from_utf8(output.stdout) {
            Err(e) => {
                return Err(ProcessError::Read {
                    msg: "failed to read stdout".to_string(),
                    source: e,
                });
            }
            Ok(val) => val,
        };
        if stdout.is_empty() {
            warn!("'{} {}' returned an empty value", program, args.join(" "));
            Err(ProcessError::EmptyResult)
        } else {
            Ok(stdout)
        }
    } else {
        Err(ProcessError::Failed {
            msg: format!(
                "'{} {}' failed to run successfully",
                program,
                args.join(" ")
            ),
            stderr,
        })
    }
}

/// Run 'op vault get VAULT_ID --format json' and return the json string
///
/// See: <https://developer.1password.com/docs/cli/reference/management-commands/vault/#vault-get>
pub async fn vault_get(vault_id: &str) -> Result<String, ProcessError> {
    let args = ["vault", "get", vault_id, "--format", "json"];
    run_cmd(&args).await
}

/// Run 'op vault list --format json' and return the json string
///
/// See: <https://developer.1password.com/docs/cli/reference/management-commands/vault/#vault-list>
pub async fn vault_list() -> Result<String, ProcessError> {
    let args = ["vault", "list", "--format", "json"];
    run_cmd(&args).await
}
