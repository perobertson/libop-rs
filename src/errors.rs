use std::string::FromUtf8Error;

use thiserror::Error;
use tokio::io;

#[derive(Debug, Error)]
pub enum ProcessError {
    #[error("empty result in response")]
    EmptyResult,
    #[error("command failed error: {msg}")]
    Failed { msg: String, stderr: String },
    #[error("io error: {msg} cmd: {cmd}")]
    Io {
        msg: String,
        cmd: String,
        source: io::Error,
    },
    #[error("reading output failed: {msg}")]
    Read { msg: String, source: FromUtf8Error },
}

#[derive(Debug, Error)]
pub enum MyError {
    #[error("running the op command failed")]
    Op { source: ProcessError },
    #[error("parsing the json response failed")]
    Parse { source: serde_json::Error },
}
